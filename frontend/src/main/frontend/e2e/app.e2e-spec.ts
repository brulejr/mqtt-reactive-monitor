import { MqttReactiveMonitorPage } from './app.po';

describe('mqtt-reactive-monitor App', () => {
  let page: MqttReactiveMonitorPage;

  beforeEach(() => {
    page = new MqttReactiveMonitorPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
