/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
package io.jrb.mqtt.monitor.service.mqtt.impl;

import static io.jrb.commons.validation.Assert.required;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.fusesource.mqtt.client.Callback;
import org.fusesource.mqtt.client.CallbackConnection;
import org.fusesource.mqtt.client.MQTT;
import org.fusesource.mqtt.client.QoS;
import org.fusesource.mqtt.client.Topic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.eventbus.EventBus;

import io.jrb.mqtt.monitor.model.StatusMessage;
import io.jrb.mqtt.monitor.model.StatusMessage.Status;
import io.jrb.mqtt.monitor.service.mqtt.MqttListenerService;

/**
 * Provides a service that listens to an MQTT source and republishes the messages onto an {@link EventBus}.
 *
 * @author <a href="mailto:brulejr@gmail.com">Jon Brule</a>
 */
public class MqttListenerServiceImpl implements MqttListenerService {

	private static final Logger LOG = LoggerFactory.getLogger(MqttListenerServiceImpl.class);
	
	private final EventBus eventBus;
	private final MQTT mqtt;
	private final List<String> topics;
	
	private final AtomicBoolean running = new AtomicBoolean();
	
	private CallbackConnection connection;
	
	public MqttListenerServiceImpl(final Supplier<MQTT> mqttSupplier, final List<String> topics, final EventBus eventBus) {
		this.mqtt = required(mqttSupplier, "mqttSupplier").get();
		this.topics = required(topics, "topics");
		this.eventBus = required(eventBus, "eventBus");
	}

	@Override
	public int getPhase() {
		return 0;
	}

	@Override
	public boolean isAutoStartup() {
		return true;
	}

	@Override
	public boolean isRunning() {
		return running.get();
	}

	@Override
	public void start() {
		LOG.info("Starting " + getClass().getSimpleName() + "...");
		connection = mqtt.callbackConnection();
		connection.listener(new MqttListener(eventBus));
		connect();
		running.getAndSet(true);
	}

	@Override
	public void stop() {
		LOG.info("Stopping " + getClass().getSimpleName() + "...");
		disconnect();
		running.getAndSet(false);
	}

	@Override
	public void stop(final Runnable callback) {
		stop();
		callback.run();
	}
	
	private void connect() {
		final Topic[] topics = this.topics.stream()
				.map(t -> new Topic(t, QoS.AT_LEAST_ONCE))
				.collect(Collectors.toList())
				.toArray(new Topic[]{});
		connection.connect(new Callback<Void>() {
			public void onSuccess(final Void v) {
				LOG.info("Successfully connected to MQTT broker");
				connection.subscribe(topics, new Callback<byte[]>() {
					
					@Override
					public void onSuccess(final byte[] qoses) {
						eventBus.post(new StatusMessage(Status.SUBSCRIBED));
					}

					@Override
					public void onFailure(final Throwable error) {
						LOG.error("Unable to subscribe to MQTT broker due to error!", error);
						eventBus.post(new StatusMessage(Status.FAILURE, error));
					}
					
				});
			}
			public void onFailure(final Throwable error) {
				LOG.error("Unable to connect to MQTT broker due to error!", error);
				eventBus.post(new StatusMessage(Status.FAILURE, error));
			}			
		});
	}
	
	private void disconnect() {
		connection.disconnect(new Callback<Void>() {
			
			@Override
			public void onSuccess(final Void v) {
				LOG.info("Successfully disconnected from MQTT broker");
				connection = null;
			}
			
			@Override
			public void onFailure(final Throwable error) {
				LOG.error("Unable to disconnect from MQTT broker due to error!", error);
				eventBus.post(new StatusMessage(Status.FAILURE, error));
				connection = null;
			}
			
		});		
	}
	
}
