/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
package io.jrb.mqtt.monitor.web.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import io.jrb.mqtt.monitor.model.Message;
import io.jrb.mqtt.monitor.model.StatusMessage;
import io.jrb.mqtt.monitor.model.StatusMessage.Status;
import io.jrb.mqtt.monitor.service.mqtt.MqttListenerService;
import reactor.core.publisher.Mono;

/**
 * Provides a reactive REST controller that manages the {@link MqttListenerService}. 
 *
 * @author <a href="mailto:brulejr@gmail.com">Jon Brule</a>
 */
@RestController
public class MessageListenerServiceController {
	
	private final MqttListenerService mqttListenerService;
	
	public MessageListenerServiceController(final MqttListenerService mqttListenerService) {
		this.mqttListenerService = mqttListenerService;
	}

	@PostMapping(value = "/start", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	Mono<Message> start() {
		if (!mqttListenerService.isRunning()) {
			mqttListenerService.start();
			return Mono.just(new StatusMessage(Status.STARTED));
		} else {
			return Mono.just(new StatusMessage(Status.RUNNING));
		}
	}

	@PostMapping(value = "/stop", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	Mono<Message> stop() {
		if (mqttListenerService.isRunning()) {
			mqttListenerService.stop();
			return Mono.just(new StatusMessage(Status.STOPPED));
		} else {
			return Mono.just(new StatusMessage(Status.NOT_RUNNING));
		}
	}
	
}
