/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
package io.jrb.mqtt.monitor.model;

import static io.jrb.commons.validation.Assert.notBlank;
import static io.jrb.commons.validation.Assert.required;

import java.util.Objects;

import org.apache.commons.lang3.builder.EqualsBuilder;

/**
 * Provide a generic data message.
 * 
 * @author <a href="mailto:brulejr@gmail.com">Jon Brule</a>
 */
public class DataMessage<T> extends MessageSupport {

	private final String topic;
	private final T data;

	public DataMessage(final String topic, final T data) {
		super();
		this.topic = notBlank(topic, "topic");
		this.data = required(data, "data");
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		DataMessage<?> rhs = (DataMessage<?>) obj;
		return new EqualsBuilder()
				.append(getId(), rhs.getId())
				.append(topic, rhs.topic)
				.append(data, rhs.data)
				.isEquals();
	}

	public String getTopic() {
		return topic;
	}

	public T getData() {
		return data;
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId(), topic, data);
	}

}
