/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
package io.jrb.mqtt.monitor.config;

import org.fusesource.mqtt.client.MQTT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.eventbus.EventBus;

import io.jrb.mqtt.monitor.log.MessageLogger;
import io.jrb.mqtt.monitor.service.mqtt.MqttConfig;
import io.jrb.mqtt.monitor.service.mqtt.MqttListenerService;
import io.jrb.mqtt.monitor.service.mqtt.impl.MqttListenerServiceImpl;
import io.jrb.mqtt.monitor.web.Jackson2WebFluxReConfigurer;

@Configuration
@ComponentScan(basePackages = "io.jrb.mqtt.monitor.web.controller")
@EnableConfigurationProperties(MqttConfig.class)
public class ApplicationJavaConfig {
	
	@Autowired
	public MqttListenerService mqttListenerService;

	@Bean
	public EventBus eventBus() {
		return new EventBus();
	}
	
	@Bean
	public MessageLogger MessageLogger(final EventBus eventBus) {
		return new MessageLogger(eventBus);
	}
	
	@Bean
	public Jackson2WebFluxReConfigurer jackson2WebFluxReConfigurer() {
		final ObjectMapper objectMapper = new ObjectMapperSupplier().get();
		return new Jackson2WebFluxReConfigurer(objectMapper);
	}

	@Bean
	public MqttListenerService mqttListenerService(final MqttConfig mqttConfig, final EventBus eventBus) {
		return new MqttListenerServiceImpl(() -> {
			final MQTT mqtt = new MQTT();
			mqtt.setHost(mqttConfig.getHost());
			mqtt.setClientId(mqttConfig.getClientId());
			mqtt.setUserName(mqttConfig.getUsername());
			mqtt.setPassword(mqttConfig.getPassword());
			return mqtt;
		}, mqttConfig.getTopics(), eventBus);
	}
	
}
