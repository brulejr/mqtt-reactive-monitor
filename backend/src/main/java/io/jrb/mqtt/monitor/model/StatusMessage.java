/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
package io.jrb.mqtt.monitor.model;

import static io.jrb.commons.validation.Assert.required;

import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.builder.EqualsBuilder;

/**
 * Provide a generic data message.
 * 
 * @author <a href="mailto:brulejr@gmail.com">Jon Brule</a>
 */
public class StatusMessage extends MessageSupport {

    public enum Status {
        CONNECTED,
        DISCONNECTED,
        FAILURE,
        NOT_RUNNING,
        RUNNING,
        STARTED,
        STOPPED,
        SUBSCRIBED;
    }
    
	private final Status status;
	private final Optional<Throwable> error;

	public StatusMessage(final Status status) {
		super();
		this.status = required(status, "status");
		this.error = Optional.empty();
	}

	public StatusMessage(final Status status, final Throwable error) {
		super();
		this.status = required(status, "status");
		this.error = Optional.of(required(error, "error"));
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		StatusMessage rhs = (StatusMessage) obj;
		return new EqualsBuilder()
				.append(getId(), rhs.getId())
				.append(status, rhs.status)
				.isEquals();
	}

	public Optional<Throwable> getError() {
		return error;
	}

	public Status getStatus() {
		return status;
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId(), status);
	}

}
