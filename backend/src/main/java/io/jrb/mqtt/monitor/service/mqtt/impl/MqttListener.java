/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
package io.jrb.mqtt.monitor.service.mqtt.impl;

import static io.jrb.commons.validation.Assert.required;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import org.fusesource.hawtbuf.Buffer;
import org.fusesource.hawtbuf.UTF8Buffer;
import org.fusesource.mqtt.client.Listener;

import com.google.common.eventbus.EventBus;

import io.jrb.mqtt.monitor.model.DataMessage;
import io.jrb.mqtt.monitor.model.StatusMessage;
import io.jrb.mqtt.monitor.model.StatusMessage.Status;

/**
 * Provides an MQTT Listener.
 *
 * @author <a href="mailto:brulejr@gmail.com">Jon Brule</a>
 */
public class MqttListener implements Listener {

	private final EventBus eventBus;

	public MqttListener(final EventBus eventBus) {
		this.eventBus = required(eventBus, "eventBus");
	}
	
	@Override
	public void onConnected() {
		eventBus.post(new StatusMessage(Status.CONNECTED));
	}

	@Override
	public void onDisconnected() {
		eventBus.post(new StatusMessage(Status.DISCONNECTED));
	}

	@Override
	public void onPublish(final UTF8Buffer topicBuffer, final Buffer bodyBuffer, final Runnable ack) {
		final String topic = topicBuffer.toString();
		try (final JsonReader jsonReader = Json.createReader(bodyBuffer.in())) {
			final JsonObject json = jsonReader.readObject();
			eventBus.post(new DataMessage<>(topic, json));
			ack.run();
		}
	}

	@Override
	public void onFailure(final Throwable error) {
		eventBus.post(new StatusMessage(Status.FAILURE, error));
	}

}
