/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
package io.jrb.mqtt.monitor.web;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import io.jrb.mqtt.monitor.model.Message;
import reactor.core.publisher.EmitterProcessor;
import reactor.core.publisher.Flux;

/**
 * Receives messages from an {@link EventBus}, making them available on a {@link Flux}.
 *
 * @author <a href="mailto:brulejr@gmail.com">Jon Brule</a>
 */
public class MessageProcessor {

	private EmitterProcessor<Message> processor;
	
	public MessageProcessor(final EventBus eventBus) {
		processor = EmitterProcessor.create();
		processor.connect();
		eventBus.register(this);
	}
	
	public Flux<Message> messageStream() {
		return processor.log();
	}
	
	@Subscribe
	public void receive(final Message message) {
		processor.onNext(message);
	}
	
}
