/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
package io.jrb.mqtt.monitor.injector;

import java.io.IOException;
import java.io.StringWriter;
import java.time.Duration;
import java.time.Instant;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.fusesource.mqtt.client.BlockingConnection;
import org.fusesource.mqtt.client.MQTT;
import org.fusesource.mqtt.client.QoS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jrb.mqtt.monitor.config.ObjectMapperSupplier;
import io.jrb.mqtt.monitor.service.mqtt.MqttConfig;
import io.moquette.server.Server;
import io.moquette.server.config.ClasspathResourceLoader;
import io.moquette.server.config.IConfig;
import io.moquette.server.config.IResourceLoader;
import io.moquette.server.config.ResourceLoaderConfig;
import reactor.core.publisher.Flux;
import reactor.util.function.Tuple2;

/**
 * Provides MQTT broker with a hot source for integration testing.
 *
 * @author <a href="mailto:brulejr@gmail.com">Jon Brule</a>
 */
@SpringBootApplication
@EnableConfigurationProperties(MqttConfig.class)
public class MqttTestInjector implements CommandLineRunner {
	
	private static final Logger LOG = LoggerFactory.getLogger(MqttTestInjector.class);

	public static void main(final String[] args) {
		new SpringApplicationBuilder(MqttTestInjector.class)
			.profiles("LOCAL")
			.run(args);		
	}
	
	@Autowired
	private Server mqttBroker;
	
	@Autowired
	private MqttConfig mqttConfig;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Bean
	public Server mqttBroker() throws IOException {
		return new Server();
	}
	
	@Bean
	public ObjectMapper objectMapper() {
		return new ObjectMapperSupplier().get();
	}
	
	@PreDestroy
	public void preDestroy() {
		LOG.info("Stopping MQTT broker...");
		mqttBroker.stopServer();
	}
	
	@PostConstruct
	public void postConstruct() throws IOException {
		LOG.info("Starting MQTT broker...");
		LOG.info("config = {}", mqttConfig);
		final IResourceLoader classpathLoader = new ClasspathResourceLoader("moquette.conf");
		final IConfig classPathConfig = new ResourceLoaderConfig(classpathLoader);
		mqttBroker.startServer(classPathConfig);
	}
	
	@Override
	public void run(final String... args) throws Exception {
		LOG.info("Making connection to MQTT...");
		final BlockingConnection connection = client();
		LOG.info("Sending events...");
		events().doOnNext(event -> {
			try {
				connection.publish("test", json(event).getBytes(), QoS.EXACTLY_ONCE, false);
			} catch (final Exception e) {
				LOG.error(e.getMessage(), e);
			}
		}).subscribe();
	}
	
	private BlockingConnection client() throws Exception {
		final MQTT mqtt = new MQTT();
		mqtt.setHost(mqttConfig.getHost());
		mqtt.setClientId(getClass().getSimpleName());
		final BlockingConnection connection = mqtt.blockingConnection();
		connection.connect();
		return connection;
	}
	
	private String json(final Event event) throws JsonGenerationException, JsonMappingException, IOException {
		final StringWriter sw = new StringWriter();
		objectMapper.writeValue(sw, event);
		return sw.toString();
	}

	private Flux<Event> events() {
		Flux<Event> eventFlux = Flux.fromStream(Stream.generate(() -> new Event(System.currentTimeMillis(), Instant.now())));		
		Flux<Long> durationFlux = Flux.interval(Duration.ofSeconds(1));
		return Flux.zip(eventFlux, durationFlux).map(Tuple2::getT1);
	}
	
}
