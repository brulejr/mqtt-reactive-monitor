CURRENT_DIR=$(pwd)
java \
  -Djava.net.preferIPv4Stack=true \
  -Dcom.sun.management.jmxremote.port=6000 \
  -Dcom.sun.management.jmxremote.authenticate=false \
  -Dcom.sun.management.jmxremote.ssl=false \
  -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=4000 \
  -jar backend/target/mqtt-reactive-monitor-app-0.0.1-SNAPSHOT.jar

